designed by Harrison Leece April 7 2024

No license

Allows drilling at 31.4 degree angle to not blow through standard sized wood boards when connecting along short edges using pocket screws
